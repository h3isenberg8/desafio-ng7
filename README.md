# Desafio NG7


## Instruções

Crie um `fork` deste projeto, e desenvolva em cima do seu fork. Use o *README.md* principal do seu repositório para nos contar como foi sua experiência em realizar o teste, o que queremos saber: 

 * As decisões que você tomou;
 * Como você organizou seu código;
 * As funcionalidades;
 * Instruções de como rodar seu projeto;
 * Dificuldades e desafios que você teve.


## Briefing

Sabemos que a tomada de decisão,. em qualquer área de negócio, é algo de extrema importância se você quiser que a sua empresa esteja em um lugar de destaque, por isso a diretoria acabou de solicitar à sua equipe para desenvolver um painel de dashboard que traga as informações dos possíveis clientes, para a expansão dos negócios da empresa em outros países:

 * Clientes por gênero;
 * Clientes por idade;
 * Quais países possuem mais possíveis clientes;
 * Quais das mulheres possuem mais de 18 anos;

Você deve usar a API abaixo para buscar as informações

`Informações randômicas dos usuários:` 
https://randomuser.me/api/?results=500

Documentação completa da API: [https://randomuser.me/documentation]

Além disso deve se basear no wireframe abaixo para construir o seu layout;

![Alt text](/src/wireframe.webp)

## O que deve ter em seu teste

* Seu sistema deve usar alguma biblioteca de gráficos (ChartJS, Plotty ou outra que você quiser);
* Deve usar alguma biblíoteca de planilhas como (Tabulator, GridJS, MaterialUi ou outro da sua escolha);
* Fazer uso do JQuery e Boostrap;
* Integração com o backend (da sua escolha) para o tratamento dos dados


### O que nós esperamos do seu teste

* Ver na solução a utilização de um framework / biblioteca da sua escolha e que você nos conte sobre essa escolha. 
* HTML5 escrito da maneira mais semântica possível
* CSS3 com um pre processador de sua escolha
* Layout responsivo
* Utilize apenas CSS/SCSS desenvolvido por você.
* Utilize HTML semântico.
* Se preocupe com a UX e UI do projeto elas serão levadas em consideração na avaliação.
* Utilize Git com boas mensagens de commit.

### O que nós ficaríamos felizes de ver em seu teste

* Alguma metodologia para definição e organização do seu código CSS

### O que nos impressionaria

* Aplicação de animações em css quando possível
* Ver o código rodando live

### O que nós não gostaríamos

* Descobrir que não foi você quem fez seu teste
* Ver commits gigantes, sem mensagens ou com -m sem pé nem cabeça


## O que avaliaremos de seu teste

* Histórico de commits do git
* As instruções de como rodar o projeto
* Organização, semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões
* Alcance dos objetivos propostos
* Componentização e extensibilidade dos componentes Javascript

## Como entrego meu teste?

Retorne o nosso e-mail, prazo máximo de 5 dias, para o e-mail vagas@ng7consultoria.com.br

## Como foi o desenvolvimento?

Apesar de ver que, a própria API ofererecia opções de query para selecionar parâmetros como idade, genero e localicação, peferi salvar os dados de requisição em uma variável global visto que cada requisição que fiz na API retornava um valor randomico! Logo, o app em seu start faz uma req e salva esses dadosem uma var globl imutáel para que sejam uzados em qualquer momento e contexto da aplicação

O atraso na entrega, apesar de injustificável, deve-se, majoritariamente à minha completa inexperiência com as ibliotecas JQuery, BootsTrap e Chartjs. Tive de ler toda a documentação das mesmas.

No geral, foi uma experiência bem gratificante pois, a medida que ia desenvolvendo o código vi que acabei deixando muita coisa redundante por erro de planejamento e pretendo rever essa técnica.

Tentei deixar o layout mais fidedigno possivel usando as ferramentas que foram pedidas e entregando o que me foi pedido além de adicionar contextos adicionais. Para rodá-lo basta abrir o index.html na pasta src.

Como fora pedidoo uso do jQuery optei por não usar nenhuma lib ou framkework.