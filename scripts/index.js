let myGrid = null;
let allUsers = [];
let gender = 'both';
let age = 'all';
let myChart = null;
let totalUserChart = null;
let underAgeUsers = null;
let genderShareChartInfo = null;

const getAllUsers = async () => {
  const allUsers = await $.getJSON('https://randomuser.me/api/?results=500');

  if (allUsers.error) {
    console.error(allUsers.error);
    return;
  }

  return allUsers;
};

const saveAllUsersToUniversalVariable = async () => {
  data = await getAllUsers();
  const result = data.results.map((e) => ({
    gender: e.gender,
    country: e.location.country,
    age: e.registered.age,
  }));

  allUsers = result;
  generateNewDomChart(result);
  createTotalShareOfUsersChart(result, result);
  createShareOfUnderAgeUsers(result);
  createShareOfMaleFemaleUsers(result);
  updateCardsInfos(result, result);
  updateGridInfos(result);
};

const sortCountriesByNumberOfPossibleClients = (selectedUsers) => {
  const countByCountry = {};

  for (const user of selectedUsers) {
    const country = user.country;

    if (country in countByCountry) {
      countByCountry[country]++;
    } else {
      countByCountry[country] = 1;
    }
  }

  const usersSortedArray = Object.entries(countByCountry).sort(
    (a, b) => b[1] - a[1]
  );

  const finalResult = usersSortedArray.map((e) => ({
    country: e[0],
    numberOfClients: e[1],
  }));

  return finalResult;
};

const generateNewDomChart = async (selectedUsers) => {
  const data = sortCountriesByNumberOfPossibleClients(selectedUsers);
  if (myChart) {
    myChart.destroy();
  }

  const ctx = document.getElementById('myChart');
  myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: data.map((row) => row.country),
      datasets: [
        {
          label: 'Number of clients per country',
          data: data.map((row) => row.numberOfClients),
        },
      ],
    },
    options: {
      plugins: {
        legend: {
          display: true,
          position: 'bottom',
        },
      },
      scales: {
        x: {
          ticks: {
            display: false,
          },
        },
      },
    },
  });
};

const createTotalShareOfUsersChart = async (
  selectedUsers,
  allAvailableUsers
) => {
  if (totalUserChart) {
    totalUserChart.destroy();
  }

  const leftOverUsersNumber = allAvailableUsers.length - selectedUsers.length;

  const data = {
    labels: ['Selected Users', 'Not selected users'],
    datasets: [
      {
        data: [selectedUsers.length, leftOverUsersNumber],
        backgroundColor: ['#6c757d', '#cbcbcc'],
        hoverOffset: 4,
      },
    ],
  };

  totalUserChart = new Chart(
    document.getElementById('totalShare'),
    {
      type: 'doughnut',
      data: data,
      options: {
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          },
        },
        scales: {
          x: {
            ticks: {
              display: false,
            },
          },
        },
      },
    }
  );
};

const createShareOfUnderAgeUsers = async (selectedUsers) => {
  if (underAgeUsers) {
    underAgeUsers.destroy();
  }

  const underAgeClients = selectedUsers.filter((e) => e.age < 18);

  const leftOverClients = selectedUsers.length - underAgeClients.length;

  const data = {
    labels: ['Share of underage', 'Share of adults'],
    datasets: [
      {
        data: [underAgeClients.length, leftOverClients],
        backgroundColor: ['#6c757d', '#cbcbcc'],
        hoverOffset: 4,
      },
    ],
  };

  underAgeUsers = new Chart(
    document.getElementById('ageChart'),
    {
      type: 'doughnut',
      data: data,
      options: {
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          },
        },
        scales: {
          x: {
            ticks: {
              display: false,
            },
          },
        },
      },
    }
  );
};

const createShareOfMaleFemaleUsers = async (selectedUsers) => {
  if (genderShareChartInfo) {
    genderShareChartInfo.destroy();
  }

  const maleUsers = selectedUsers.filter((e) => e.gender == 'male');
  const maleUserNumb = maleUsers.length;
  const femaleUserNumb = selectedUsers.length - maleUserNumb;

  const data = {
    labels: ['Share of males', 'Share of females'],
    datasets: [
      {
        data: [maleUserNumb, femaleUserNumb],
        backgroundColor: ['#6c757d', '#cbcbcc'],
        hoverOffset: 4,
      },
    ],
  };

  genderShareChartInfo = new Chart(
    document.getElementById('genderChart'),
    {
      type: 'doughnut',
      data: data,
      options: {
        plugins: {
          legend: {
            display: true,
            position: 'bottom',
          },
        },
        scales: {
          x: {
            ticks: {
              display: false,
            },
          },
        },
      },
    }
  );
};

const listenToSetupButtons = async () => {
  const genderButton = $('#gender');
  genderButton.on('change', (event) => {
    const chosenGender = event.target.value;
    let selectedUsers = [];

    if (age === 'all') {
      selectedUsers = allUsers.filter(
        (user) => user.gender === chosenGender || chosenGender === 'both'
      );
    } else if (age === 'adult') {
      selectedUsers = allUsers
        .filter(
          (user) => user.gender === chosenGender || chosenGender === 'both'
        )
        .filter((user) => user.age >= 18);
    } else {
      selectedUsers = allUsers
        .filter(
          (user) => user.gender === chosenGender || chosenGender === 'both'
        )
        .filter((user) => user.age < 18);
    }

    gender = chosenGender;

    generateNewDomChart(selectedUsers);
    createTotalShareOfUsersChart(selectedUsers, allUsers);
    createShareOfUnderAgeUsers(selectedUsers);
    createShareOfMaleFemaleUsers(selectedUsers);
    updateCardsInfos(allUsers, selectedUsers);
    updateGridInfos(selectedUsers);
  });

  const ageButton = $('#age');
  ageButton.on('change', (event) => {
    const chosenAge = event.target.value;
    let selectedUsers = [];

    if (chosenAge == 'underage') {
      selectedUsers = allUsers.filter((e) => e.age < 18);
      age = 'underage';
    }

    if (chosenAge == 'adult') {
      selectedUsers = allUsers.filter((e) => e.age >= 18);
      age = 'adult';
    }

    if (chosenAge == 'all') {
      selectedUsers = allUsers;
      age = 'all';
    }

    if (gender == 'male') {
      selectedUsers = selectedUsers.filter((e) => e.gender == 'male');
    }

    if (gender == 'female') {
      selectedUsers = selectedUsers.filter((e) => e.gender == 'female');
    }

    if (gender == 'both') {
      selectedUsers = selectedUsers;
    }

    generateNewDomChart(selectedUsers);
    createTotalShareOfUsersChart(selectedUsers, allUsers);
    createShareOfUnderAgeUsers(selectedUsers);
    createShareOfMaleFemaleUsers(selectedUsers);
    updateCardsInfos(allUsers, selectedUsers);
    updateGridInfos(selectedUsers);
  });
};

const updateCardsInfos = (allAvailableUsers, selectedUsers) => {
  $('#totalNumberOfClientsCard').text(String(allAvailableUsers.length));

  $('#selectedNumberOfClientsCard').text(String(selectedUsers.length));

  const clientsByCountry = sortCountriesByNumberOfPossibleClients(
    selectedUsers
  );

  $('#countryWithBiggestNumberOfClientsCard').text(
    String(clientsByCountry[0].country)
  );

  $('#countryWithSmallestNumberOfClientsCard').text(
    String(clientsByCountry[clientsByCountry.length - 1].country)
  );
};

const updateGridInfos = (selectedUsers) => {
  const data = selectedUsers.map((e) => [
    String(e.country),
    String(e.age),
    String(e.gender),
  ]);

  if (myGrid) {
    myGrid.updateConfig({
      data: data,
    });
    myGrid.forceRender();

    return;
  }

  myGrid = new gridjs.Grid({
    columns: ['Country', 'Age', 'Gender'],
    data: data,
    pagination: {
      limit: 2,
    },
  });

  myGrid.render(document.getElementById('wrapper'));
};

saveAllUsersToUniversalVariable();
listenToSetupButtons();
